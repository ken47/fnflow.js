/*
 The MIT License (MIT)

 Copyright (c) 2014 Kenneth Hahn

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

var FnFlow = (function() {
  var INT_MODULO = Math.pow(2, 54);

  // make static property of class
  var Status = {
    CLOSED: 0,
    RUNNING: 1,
    TAKING: 2,
    KILLED: 3,
    ERROR: 4,
    PAUSED: 5
  };

  /**
   *
   * @param {string} prop
   * @returns {string}
   * @private
   */
  function _getPropertySuffix(prop) {
    var withoutUnderscore = prop.charAt(0) === "_" ? prop.slice(1) : prop;
    return withoutUnderscore.slice(0, 1).toUpperCase() + withoutUnderscore.slice(1);
  }

  /**
   * @param {function} constructor
   * @param {BaseObject} properties
   */
  function bootstrapConstructor(constructor, properties) {
    for(var i = properties.length; i--;) {
      (function(prop) {
        var suffix = _getPropertySuffix(prop);
        var getter = 'get' + suffix;
        var setter = 'set' + suffix;
        var unsetter = '_unset' + suffix;

        constructor.prototype[getter] = constructor.prototype[getter] || function() {
          return this[prop];
        };

        constructor.prototype[setter] = constructor.prototype[setter] || function(val) {
          this[prop]  = val;
          return this;
        };

        constructor.prototype[unsetter] = constructor.prototype[unsetter] || function(val) {}
      })(properties[i]);
    }

    constructor.prototype.deconstruct = constructor.prototype.deconstruct || function() {
      if (constructor.prototype.beforeDeconstruct) {
        constructor.prototype.beforeDeconstruct();
      }
      for(var i = properties.length; i--;) {
        var prop = properties[i];
        var suffix = _getPropertySuffix(prop);
        this['_unset' + suffix]();
        this[prop] = null;
      }
    }
  };

  function setProperties(instance, properties, overwrittenProps) {
    for(var prop in properties) {
      if (overwrittenProps && ({}).hasOwnProperty.call(overwrittenProps, prop)) {
        instance[prop] = overwrittenProps[prop];
      } else {
        instance[prop] = properties[prop];
      }
    }
  }

  var tests = {
    bootstrapConstructor: function() {
      function FooBar() {}

      bootstrapConstructor(FooBar, ['_foo', 'hello']);

      var f1 = new FooBar();
      f1.setFoo(1);
      console.assert(f1.getFoo() === 1);
      f1.setHello('world');
      console.assert(f1.getHello() === 'world');
    },

    setProperties: function() {
      var properties = {
        hello: 'world',
        foo: 'bar'
      }

      function Constructor() {
        setProperties(this, properties);
      }
      var cons = new Constructor();
      console.assert(cons.hello === 'world');
      console.assert(cons.foo === 'bar');
    }
  };

  var LinkedList = (function() {
    /*
     use case to watch out for.
     observer to be added to multiple sequences, and thus
     must be able to reference and remove themselves from
     multiple lists
     */
    var Node = (function() {
      var DEFAULT_ATTRIBUTES = {
        _val: null,
        _next: null,
        _prev: null,
        _linkedList: null
      };

      bootstrapConstructor(Node, Object.keys(DEFAULT_ATTRIBUTES));

      function Node(val, linkedList) {
        setProperties(this, DEFAULT_ATTRIBUTES);
        this.setVal(val);
        this.setLinkedList(linkedList);
      }

      return Node;
    })();

    var LinkedListIterator = (function() {
      var DEFAULT_ATTRIBUTES = {
        _linkedList: null,
        _currentNode: null
      };

      function Iterator(linkedList, startNode) {
        this.setLinkedList(linkedList);
        if (startNode) {
          this.setCurrentNode(startNode.getPrev());
        } else {
          this.setCurrentNode(linkedList.getRoot());
        }
      }

      Iterator.prototype = {
        hasNext: function() {
          return !this.getLinkedList().isRoot(this.getCurrentNode().getNext());
        },

        next: function() {
          var node = this.getCurrentNode().getNext();
          this.setCurrentNode(node);
          return node.getVal();
        }
      };

      bootstrapConstructor(Iterator, Object.keys(DEFAULT_ATTRIBUTES));

      return Iterator;
    })();

    var DEFAULT_ATTRIBUTES = {
      _root: null,
      _size: 0
    };

    function LinkedList() {
      setProperties(this, DEFAULT_ATTRIBUTES);
      var root = new Node(null, this);
      root.setPrev(root);
      root.setNext(root);
      this.setRoot(root);
    }

    LinkedList.prototype = {
      reverse: function() {
        //todo
      },

      clone: function() {
        var list = new LinkedList();

        this.forEach(function(val) {
          list.append(val);
        });

        return list;
      },

      map: function(callback) {
        var list = new LinkedList();

        this.forEach(function(val) {
          list.append(callback(val));
        });

        return list;
      },

      toArray: function() {
        var ret = [];
        this.forEach(function(val, idx) {
          ret.push(val);
        });
        return ret;
      },

      forEach: function(cb) {
        var idx = 0;
        var iterator = this.iterator();
        while(iterator.hasNext()) {
          var val = iterator.next();
          cb(val, idx++);
        }
      },

      /**
       * @param startNode
       * @returns {LinkedListIterator}
       */
      iterator: function(startNode) {
        return new LinkedListIterator(this, startNode);
      },

      addBefore: function(val, next) {
        var node = new Node(val, this);
        var prev = next.getPrev();

        node.setPrev(prev);
        node.setNext(next);
        next.setPrev(node);
        prev.setNext(node);

        this.setSize(this.getSize() + 1);

        return node;
      },

      append: function(val) {
        return this.addBefore(val, this.getRoot());
      },

      removeHead: function() {
        if (this.isEmpty()) throw new FnFlow.Error("List is empty.");
        else {
          return this.remove(this.getRoot().getNext());
        }
      },

      remove: function(node) {
        if (this.isEmpty()) {
          throw new FnFlow.Error("List is empty.");
        } else if (node.getLinkedList() !== this) {
          throw new FnFlow.Error("Node does not belong to this list.");
        } else {
          this.setSize(this.getSize() - 1);

          var prev = node.getPrev();
          var next = node.getNext();
          prev.setNext(next);
          next.setPrev(prev);

          return node.getVal();
        }
      },

      peekHead: function() {
        if (this.isEmpty()) return null;
        else {
          return this.getRoot().getNext().getVal();
        }
      },

      isRoot: function(node) {
        return node === this.getRoot();
      },

      isEmpty: function() {
        return this.getSize() === 0;
      }
    };

    bootstrapConstructor(LinkedList, Object.keys(DEFAULT_ATTRIBUTES));

    return LinkedList;
  })();

  tests.LinkedList = function() {
    var list = new LinkedList();

    console.assert(list.isEmpty() === true, "list is empty");

    list.append(1);

    console.assert(list.isEmpty() === false, "list is not empty");

    list.append(2);
    var threeNode = list.append(3);
    list.append(4);

    console.assert(list.getSize() === 4, 'list size is 4');

    var iterator = list.iterator();
    var values = [];
    while(iterator.hasNext()) {
      values.push(iterator.next());
    }

    console.assert(values.length === 4, 'iterator returned all values');
    console.assert(values.length === list.getSize(), 'length and size are equal');
    for(var i = 0; i < values.length; ++i) {
      console.assert(values[i] === i + 1, 'has appropriate value for each index');
    }

    list.forEach(function(val, i) {
      console.assert(val === i + 1, 'has appropriate value for each index');
    });

    list.remove(threeNode);
    console.assert(list.getSize() === 3, "list size was reduced");

    var iterator = list.iterator();
    var values = [];
    while(iterator.hasNext()) {
      values.push(iterator.next());
    }

    console.assert(values.length === 3, 'contained 3 values');
    for(var i = 0; i < values.length; ++i) {
      console.assert(values[i] !== 3, 'three no longer in list');
    }

    console.assert(1 === list.removeHead(), 'expected head of list');
    console.assert(2 === list.removeHead(), 'expected head of list');
    console.assert(4 === list.removeHead(), 'expected head of list');

    console.assert(list.isEmpty() === true, 'list is empty');

    list.append(5);

    console.assert(list.isEmpty() === false, 'list is no longer empty');
    console.assert(5 === list.peekHead(), '5 is head of list');
  }

  var LinkedHashMap = (function() {
    var DEFAULT_ATTRIBUTES = {
      _linkedList: null,
      _hashMap: null,
      _nodeMap: null
    };

    function LinkedHashMap() {
      setProperties(this, DEFAULT_ATTRIBUTES);
      this.setLinkedList(new LinkedList());
      this.setHashMap({});
      this.setNodeMap({});
    }

    LinkedHashMap.prototype = {
      //todo: more elegant wayt o delegate?
      forEach: function(callback) {
        var hashMap = this.getHashMap();

        this.getLinkedList().forEach(function(key, idx) {
          callback(hashMap[key], idx);
        }.bind(this));
      },

      map: function(callback) {
        var hashMap = this.getHashMap();

        return this.getLinkedList().map(function(key, idx) {
          callback(hashMap[key], idx);
        }.bind(this));
      },

      filter: function(callback) {
        var hashMap = this.getHashMap();

        return this.getLinkedList().filter(function(key, idx) {
          callback(hashMap[key], idx);
        }.bind(this));
      },

      forEachKey: function(callback) {
        var hashMap = this.getHashMap();

        this.getLinkedList().forEach(function(key, idx) {
          callback(key, idx);
        }.bind(this));
      },

      keyIterator: function(startNode) {
        return this.getLinkedList().iterator(startNode);
      },

      getSize: function() {
        return this.getLinkedList().getSize();
      },

      isEmpty: function() {
        return this.getLinkedList().isEmpty();
      },

      put: function(key, val) {
        var hashMap = this.getHashMap();
        var nodeMap = this.getNodeMap();

        if (!({}).hasOwnProperty.call(hashMap, key)) {
          var node = this.getLinkedList().append(key);
          nodeMap[key] = node;
        }
        hashMap[key] = val;
      },

      get: function(key) {
        return this.getHashMap()[key];
      },

      containsKey: function(key) {
        return ({}).hasOwnProperty.call(this.getHashMap(), key);
      },

      remove: function(key) {
        var hashMap = this.getHashMap();
        var nodeMap = this.getNodeMap();

        if (!({}).hasOwnProperty.call(hashMap, key)) return void 0;
        else {
          var node = nodeMap[key];
          var ret = hashMap[key];
          delete hashMap[key];
          delete nodeMap[key];
          this.getLinkedList().remove(node);
          return ret;
        }
      },

      containsKey: function(key) {
        var hashMap = this.getHashMap();
        return ({}).hasOwnProperty.call(hashMap, key);
      }
    };

    bootstrapConstructor(LinkedHashMap, Object.keys(DEFAULT_ATTRIBUTES));

    return LinkedHashMap;
  })();

  tests.LinkedHashMap = function() {
    var hashMap = new LinkedHashMap();

    console.assert(hashMap.isEmpty() === true, "hashMap is empty");

    hashMap.put('one', 1);
    console.assert(hashMap.isEmpty() === false, "hashMap is not empty");

    hashMap.put('two', 2);
    hashMap.put('three', 3);
    hashMap.put('four', 4);

    console.assert(hashMap.getSize() === 4, 'hashMap size is 4');

    hashMap.forEach(function(val, i) {
      console.assert(val === i + 1, 'has appropriate value for each index');
    });

    hashMap.remove('three');
    console.assert(hashMap.getSize() === 3, "hashMap size was reduced");

    hashMap.remove('one');
    hashMap.remove('two');
    hashMap.remove('four');

    console.assert(hashMap.isEmpty() === true, 'hashMap is empty');

    hashMap.forEach(function(val, idx) {
      console.assert(false, 'this should never run');
    });

    hashMap.put('five', 5);

    console.assert(hashMap.isEmpty() === false, 'hashMap is no longer empty');
    console.assert(5 === hashMap.get('five'), 'value 5 at key "five"');
  }

  var FnFlow = (function() {
    var DEFAULT_PROPERTIES = {
      _closingPid: null,
      _concurrency: 1,
      _continueOnError: true,
      _id: null,
      _initialState: null,
      _list: null,
      _listIterator: null,
      _observableMap: null,
      _observerMap: null,
      _opQueue: null,
      _processCount: 0,
      _processPool: null,
      //todo
      _safeMode: true,
      _sourceFlows: null,
      _status: Status.CLOSED,
      _targetFlowsData: null
    };

    function FnFlow(config) {
      setProperties(this, DEFAULT_PROPERTIES, config);
      this.setList(new LinkedList());
      this.setObserverMap(new LinkedHashMap());
      this.setProcessPool(new LinkedHashMap());
      this.setId(FnFlow.getNextId());
      this.setListIterator(this.getList().iterator());
      this.setSourceFlows(new LinkedHashMap());
      this.setTargetFlowsData(new LinkedHashMap());
      this.setInitialState(null || config._initialState);
    }

    FnFlow.currentId = 0;

    FnFlow.getNextId = function() {
      return FnFlow.currentId++ % INT_MODULO;
    }

    FnFlow.prototype = {
      // private
      // todo: pass in the process as well as the err,
      // and set resultPool[process] to FnFlow.Error
      _onError: function(process) {
        this._onProcessComplete(process);

        if (!this.getContinueOnError()) {
          this.stopFlowAfter(process);
        }
      },

      _unsetProcessPool: function() {
        var processPool = this.getProcessPool();
        processPool.forEach(function(p) {
          console.log('process', p);
          p.kill();
        });
      },

      _onProcessComplete: function(process) {
        if (this.getStatus() === Status.CLOSED) return;

        this._tryToReportResult(process);
        this._nextProcess();
      },

      // this method should only be called if taking
      // todo: reset takeCount and takeUntil some time
      _shouldStopProcessing: function() {
        // isListFinished
        // todo: check if status.Taking
        return this._isListFinished() || this.getStatus() === Status.CLOSED;
      },

      _isListFinished: function() {
        if (this.getStatus() === Status.RUNNING) return false;
        else return this.getList().length === 0;
      },

      _isProcessPoolFull: function() {
        var concurrency = this.getConcurrency();
        return concurrency === this.getProcessPool().getSize();
      },

      /**
       * start of loop
       *
       * @private
       */
      _nextProcess: function() {
        // todo distinction between shouldStop and shouldComplete?
        if (!this._shouldStopProcessing()) {
          var iterator = this.getListIterator();

          if (!iterator.hasNext() || this._isProcessPoolFull()) {
            return;
          } else {
            var process = FnFlow.Process.spawn(this);
            var startVal = iterator.next();
            process.start(startVal);

            // todo: handle ordering of results
            this._nextProcess();
          }
        }
      },

      _getNextProcessIdx: function() {
        var idx = this.getProcessCount();
        this.setProcessCount(idx + 1);
        return idx;
      },

      _addToProcessPool: function(process) {
        var processPool = this.getProcessPool();
        var id = process.getId();
        processPool.put(id, process.getSummary());
      },

      _onFiltered: function(process) {
        this._removeFromPoolsById(process.getId());
        this._nextProcess();
      },

      _removeFromPools: function(process) {
        this._removeFromPoolsById(process.getId());
      },

      _removeFromPoolsById: function(pid) {
        var processPool = this.getProcessPool();
        processPool.remove(pid);
      },

      /**
       * @param {?boolean} forceReport
       * @returns {Array}
       * @private
       */
      _getReportableResults: function(forceReport) {
        var ret = [];
        var processPool = this.getProcessPool();

        var keyIterator = processPool.keyIterator();
        while(keyIterator.hasNext()) {
          var key = keyIterator.next();
          // todo: ensure error case is handled
          if (processPool.get(key).result === PendingResult) {
            if (forceReport) continue;
            else break;
          } else {
            ret.push(key);
          }
        }

        return ret;
      },

      /**
       *
       * @param process
       * @param forceReport
       * @private
       */
      _tryToReportResult: function(process, forceReport) {
        var id = process.getId();
        var processPool = this.getProcessPool();
        processPool.get(id).result = process.getResult();

        var reportableResults = this._getReportableResults(forceReport);

        for(var i = 0, len = reportableResults.length; i < len; ++i) {
          var pid = reportableResults[i];
          this._reportResult(pid);
          this._removeFromPoolsById(pid);

          if (pid === this.getClosingPid()) {
            this._onComplete();
          }
        }
      },

      _reportResult: function(pid) {
        var processPool = this.getProcessPool();
        var processResult = processPool.get(pid).result;

        var observerMap = this.getObserverMap();

        observerMap.forEach(function(observer) {
          if (processResult instanceof FnFlow.Error === false) {
            observer.notifyResult(processResult);
          } else {
            observer.notifyError(processResult);
          }
        });

        processPool.remove(pid);
      },

      /**
       * kills processes that were initiated after
       *   the passed in process. or kills all processes
       *   if no process is passed in.
       *
       * @param {?FnFlow.Process} process
       */
      stopFlowAfter: function(process) {
        var processPool = this.getProcessPool();

        // in case we're trying to stop the flow after a process that has already ended
        var found = !processPool.containsKey(process.getId());

        // todo: did not check thoroughly for race conditions with this .close() call.
        //   should be safe so long as a process is not started before being placed in
        //   a process pool.
        // todo: can be optimized by implementing reverse iterator
        processPool.forEach(function(p) {
          if (found) {
            p.process.kill();
            this._removeFromPools(p.process);
          } else if (p.process === process) {
            found = true;
            this.setClosingPid(p.id);
          };
        }.bind(this));
      },

      /*
       todo: flesh out. note that this is not called for infinite sequences
       unless there is an error or a take condition is set
       */

      _onComplete: function() {
        this.setStatus(Status.CLOSED);
        var observerMap = this.getObserverMap();
        observerMap.forEach(function(observer) {
          observer.notifyComplete();
        });
      },

      /**
       * todo: open all sources and forks?
       *
       * @param {?BaseObject} observerConfig - arg one would pass into FnFlow.Observer constructor
       * @returns {*}
       */
      open: function(observerConfig) {
        if (this.getStatus() === Status.RUNNING) return;

        if (observerConfig) {
          this.addObserver(new FnFlow.Observer(observerConfig));
        }
        this.setClosingPid(null);
        this.setStatus(Status.RUNNING);
        this._nextProcess();
        return this;
      },

      /**
       * opens all attached flows.
       *
       * @param {?BaseObject} observerConfig - arg one would pass into FnFlow.Observer constructor
       */
      openAll: function(observerConfig) {
        this.open(observerConfig);
        this._openAll({});
        return this;
      },

      _openAll: function(seen) {
        this.open();

        this.getSourceFlows().forEach(function(flow) {
          var flowId = flow.getId();
          if (seen[flowId]) return;
          seen[flowId] = true;
          flow._openAll(seen);
        });

        this.getTargetFlowsData().forEach(function(targetData) {
          var target = targetData.flow;
          var targetId = target.getId();
          if (seen[targetId]) return;
          seen[targetId] = true;
          target._openAll(seen);
        });
      },

      addObserver: function(observer) {
        observer.addBroker(this);
        this.getObserverMap().put(observer.getId(), observer);
        return this;
      },

      removeObserver: function(observer) {
        observer.removeBroker(this);
        this.getObserverMap().remove(observer.getId());
        return this;
      },

      // todo flesh out
      close: function() {
        this.setStatus(Status.CLOSED);
        return this;
      },

      destroy: function() {
        this.close();
        // todo: detach all references
      },

      addItem: function(item) {
        this.getList().append(item);
        this._nextProcess();
        return this;
      },

      addList: function(list) {
        for(var i = 0, len = list.length; i < len; ++i) {
          this.addItem(list[i]);
        }
        return this;
      },

      //todo: test
      // todo -- expand to other kinds of sources
      addSourceFlow: function(source, adapter) {
        var sourceFlows = this.getSourceFlows();
        var sourceId = source.getId();
        if (sourceFlows.containsKey(sourceId)) return;
        sourceFlows.put(sourceId, source);
        source.addTargetFlow(this, adapter);
        return this;
      },

      //todo: test
      removeSourceFlow: function(source) {
        var sourceFlows = this.getSourceFlows();
        var sourceId = source.getId();
        if (!sourceFlows.containsKey(sourceId)) return;
        sourceFlows.remove(sourceId);
        source.removeTargetFlow(this);
        return this;
      },

      //todo: test
      // todo: expose an addError and addComplete(?) endpoint
      // to be passed by observer
      /**
       *
       * @param {FnFlow} target
       * @param {?function} adapter - takes a result and transforms it into a
       *   version suitable for use by the destination source
       * @returns {FnFlow.Observer}
       */
      addTargetFlow: function(target, adapter) {
        var targetFlows = this.getTargetFlowsData();
        var targetId = target.getId();
        if (targetFlows.containsKey(targetId)) return;

        var obs = new FnFlow.Observer({
          onResult: function(result) {
            target.addItem(adapter ? adapter(result) : result);
          }
        });
        targetFlows.put(targetId, {
          flow: target,
          observer: obs
        });

        this.addObserver(obs);
        target.addSourceFlow(this);
        return this;
      },

      //todo: test
      //todo untested
      removeTargetFlow: function(target) {
        var targetFlows = this.getTargetFlowsData();
        var targetId = target.getId();
        if (!targetFlows.containsKey(targetId)) return;
        var targetData = targetFlows.remove(targetId);
        this.removeObserver(targetData.observer);
        return this;
      }
    }

    bootstrapConstructor(FnFlow, Object.keys(DEFAULT_PROPERTIES));

    return FnFlow;
  })();

  var OpCbWrapper = (function() {
    var DEFAULT_PROPERTIES = {
      _called: false,
      _parent: null,
      _process: null
    };

    function Wrapper(process, parent) {
      this.setParent(parent);
      this.setProcess(process);
      this._wrapCallbacks();
    }

    Wrapper.create = function(process, parent) {
      return new Wrapper(process, parent);
    }

    Wrapper.prototype = {
      _wrapCallbacks: function() {
        var process = this.getProcess();
        var callbacks = ['onEach', 'onError', 'onFilter', 'onMap'];

        for(var i = callbacks.length; i--;) {
          (function(cbName) {
            this[cbName] = function() {
              if (this.getCalled()) {
                throw new FnFlow.Error("Can only execute callback once per operation.\n" +
                  "Callback: " + cbName + ".\n" +
                  "Operation: " + this.getParent().getType());
              }
              this.setCalled(true);
              process[cbName].apply(process, arguments);
            }.bind(this);
          }).bind(this)(callbacks[i]);
        }
      }
    }

    bootstrapConstructor(Wrapper, Object.keys(DEFAULT_PROPERTIES));

    return Wrapper;
  })();

  FnFlow.Ops = (function() {
    var Ops = {};
    function SharedConstructor(arg) {
      this.setArg(arg);
    }

    bootstrapConstructor(SharedConstructor, ['_arg', '_type']);

    function extend(constructor) {
      constructor.prototype = new SharedConstructor();
    }

    var types = ['Each', 'AsyncEach', 'Filter', 'AsyncFilter', 'Map', 'AsyncMap', 'Take'];

    for(var i = types.length; i--;) {
      (function(type) {
        Ops[type] = function() {
          SharedConstructor.apply(this, arguments);
          this.setType(type);
          this.init();
        }
        extend(Ops[type]);
        Ops[type].prototype.init = function() {}
      })(types[i]);
    }

    Ops.Each.prototype.process = function(currentVal, process) {
      process.getState();

      this.getArg()(currentVal, process.getState());
      process.onEach();
    }

    Ops.AsyncEach.prototype.process = function(currentVal, process) {
      var wrapper = OpCbWrapper.create(process, this);
      this.getArg()(currentVal, wrapper.onEach, wrapper.onError, process.getState());
    }

    Ops.Filter.prototype.process = function(currentVal, process) {
      return process.onFilter(this.getArg()(currentVal, process.getState()));
    }

    Ops.AsyncFilter.prototype.process = function(currentVal, process) {
      var wrapper = OpCbWrapper.create(process, this);
      return this.getArg()(currentVal, wrapper.onFilter, wrapper.onError, process.getState());
    }

    Ops.Map.prototype.process = function(currentVal, process) {
      return process.onMap(this.getArg()(currentVal, process.getState()));
    }

    Ops.AsyncMap.prototype.process = function(currentVal, process) {
      var wrapper = OpCbWrapper.create(process, this);
      return this.getArg()(currentVal, wrapper.onMap, wrapper.onError, process.getState());
    }

    // todo: refactor to work with concurrency > 1
    Ops.Take.prototype.init = function() {
      this.setType('take');
      this.taken = 0;
    }
    Ops.Take.prototype.process = function(currentVal, process) {
      var arg = this.getArg();
      var limit = typeof arg === 'number' ? arg : arg(process.getState());
      ++this.taken;

      if (this.taken === limit) process.stopFlow();
      if (this.taken > limit) return process.kill();
      else return process._nextOp();
    }

    return Ops;
  })();

  var PendingResult = {};

  FnFlow.Process = {
    spawn: function(flow) {
      return new Process(flow);
    }
  }

  var ProcessState = (function() {
    function State(initialState) {
      this._val = initialState;
    }

    State.prototype = {
      set: function(val) {
        this._val = val;
      },

      get: function() {
        return this._val;
      }
    }

    return State;
  })();

  var Process = (function() {
    var DEFAULT_ATTRIBUTES = {
      currentVal: null,
      id: null,
      originalVal: null,
      opIterator: null,
      parent: null,
      state: null,
      status: Status.CLOSED
    };

    function Process(flow) {
      if (flow instanceof FnFlow === false) {
        throw FnFlow.Error("Invalid parent sequence for Process.");
      }

      setProperties(this, DEFAULT_ATTRIBUTES);
      this.setId(flow._getNextProcessIdx());
      this.setParent(flow);
      this.setState(new ProcessState(flow.getInitialState()));
      flow._addToProcessPool(this);

      this.setOpIterator(flow.getOpQueue().iterator());
    }

    // todo: composition to access key parent instance variables
    Process.prototype = {
      start: function(initialValue) {
        // todo: clone if object
        this.setOriginalVal(initialValue);
        this.setCurrentVal(initialValue);
        this.setStatus(Status.RUNNING);
        this._nextOp();
      },

      kill: function() {
        this.setParent(null);
        this.setStatus(Status.KILLED);
      },

      //todo
      pause: function() {
        this.setStatus(Status.PAUSED);
      },

      //todo
      resume: function() {
        this.setStatus(Status.RUNNING);
      },

      getSummary: function() {
        return {
          id: this.getId(),
          originalVal: this.getOriginalVal(),
          process: this,
          result: PendingResult
        }
      },

      getResult: function() {
        if (this.getStatus() === Status.RUNNING) {
          throw new FnFlow.Error('Attempting to access ' +
            'process result before the process has completed');
        }

        return this.getCurrentVal();
      },

      _nextOp: function() {
        var iterator = this.getOpIterator();

        if (this.getStatus() !== Status.RUNNING) return;
        else if (!iterator.hasNext()) {
          this.setStatus(Status.CLOSED);
          return this.getParent()._onProcessComplete(this);
        } else {
          return this._processOp(iterator.next());
        }
      },

      stopFlow: function() {
        this.getParent().stopFlowAfter(this);
        // stop flow from parent
      },

      onMap: function(val) {
        this.setCurrentVal(val);
        this._nextOp();
      },

      onFilter: function(bool) {
        if (bool) this._nextOp();
        else {
          this.getParent()._onFiltered(this);
          this._end();
        }
      },

      onEach: function() {
        this._nextOp();
      },

      // exit point
      onError: function(err) {
        if (this.getStatus() !== Status.RUNNING) return;

        this.setStatus(Status.ERROR);
        this.setCurrentVal(err);
        // todo: _onError here must know that the process is over
        this.getParent()._onError(this);
        this._end();
      },

      // exit point
      _nextProcess: function() {
        this.getParent()._nextProcess();
        this._end();
      },

      _end: function() {
        if (this.getStatus() === Status.RUNNING) this.setStatus(Status.CLOSED);
        this.setParent(null);
        this.setOpIterator(null);
      },

      _processOp: function(op) {
        var parent = this.getParent();

        // todo: should this variable with fully contained in Process?
        // todo: is this a sufficient 'stop' mechanism?
        if (parent.getStatus() === Status.CLOSED) {
          parent._nextProcess();
          this._end();
        } else {
          try {
            op.process(this.getCurrentVal(), this);
          } catch(e) {
            this.onError(FnFlow.Error.fromError(e));
          }
        }
      }
    };

    bootstrapConstructor(Process, Object.keys(DEFAULT_ATTRIBUTES));

    return Process;
  })();

  // todo fill out
  FnFlow.Error = (function() {
    var FnFlowError = function(message, name, stack) {
      this.name = name || 'FnFlow.Error';
      this.message = message;
      this.stack = stack || (new Error()).stack;

      if (stack) this.message += "\nOriginal Stack:\n" + stack.split("\n");
    }

    FnFlowError.fromError = function(err) {
      return new FnFlowError(err.message, err.name, err.stack);
    }

    FnFlowError.prototype = new Error();

    return FnFlowError;
  })();

  /*

   * decouple building from other canal operations
   *
   * non-build ops:
   * - open
   * - close
   * - take
   * - throttle
   * - ignoreDuplicates
   * - addObserver
   *
   * build ops
   * - all the functional operators
   * - addObserver
   * - addPromise?
   */
  /**
   * todo: independent object clone method
   *
   *
   * @param this.state
   * @returns {{each: Function, map: Function, filter: Function, aEach: Function, aMap: Function, aFilter: Function, take: Function, start: Function, stop: Function}}
   * @constructor
   */
  FnFlow.Builder = (function() {
    var DEFAULT_PROPERTIES = {
      _opQueue: null
    };

    /**
     * @param {?LinkedList} opQueue - used for copy constructor
     * @constructor
     */
    function Builder(opQueue) {
      setProperties(this, DEFAULT_PROPERTIES);
      if (opQueue) {
        if (opQueue instanceof LinkedList === false) {
          throw new FnFlow.Error("Invalid constructor argument.");
        }
        this.setOpQueue(opQueue);
      } else {
        this.setOpQueue(new LinkedList());
      }
    }

    Builder.spawn = function() {
      return new Builder();
    }

    Builder.prototype = {
      addOp: function(op) {
        this.getOpQueue().append(op);
        return this;
      },

      each: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.Each(callback);
        });
      },

      map: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.Map(callback);
        });
      },

      filter: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.Filter(callback);
        });
      },

      aEach: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.AsyncEach(callback);
        });
      },

      aMap: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.AsyncMap(callback);
        });
      },

      aFilter: function(callback) {
        return this.addOp(function() {
          return new FnFlow.Ops.AsyncFilter(callback);
        });
      },

      take: function(n) {
        return this.addOp(function() {
          return new FnFlow.Ops.Take(n);
        });
      },

      //todo
      flatMap: function() {},

      //todo
      aFlatMap: function() {},

      clone: function() {
        return new Builder(this.getOpQueue().slice());
      },

      build: function(initialState) {
        var fnFlow = new FnFlow({
          _opQueue: this.getOpQueue().map(function(opBuilder) {
            return opBuilder();
          }),
          _initialState: initialState
        });

        return fnFlow;
      }
    };

    bootstrapConstructor(Builder, Object.keys(DEFAULT_PROPERTIES));

    return Builder;
  })();

  tests.Builder = function() {
    var builder = FnFlow.Builder.spawn()
      .each(function(val, state) {
        state.set(1);
      })
      .filter(function(val, state) {
        state.set(state.get() + 1);
        return true;
      })
      .map(function(val, state) {
        state.set(state.get() + 1);
        return val * 2;
      })
      .aEach(function(val, onEach, errback, state) {
        state.set(state.get() + 1);
        setTimeout(function() {
          onEach();
        }, Math.random() * 20);
      })
      .aFilter(function(val, onFilter, errback, state) {
        state.set(state.get() + 1);
        onFilter(true);
      })
      .aMap(function(val, onMap, errback, state) {
        state.set(state.get() + 1);
        onMap(val + 1);
      })
      .each(function(val, state) {
        state.set(state.get() + 1);
      })
      .take(6);

    var flow = builder.build()
      .setContinueOnError(true)
      //      .setConcurrency(4)
      .open({
        onResult: function(result) {
          console.log('result', result);
        },

        onError: function(err) {
          console.log(err);
        },

        onComplete: function(results) {
          console.log('all results', results);
          flow.deconstruct();
        }
      })
      .addList([1, 2, 3, 4, 5]);

    console.assert(flow.getOpQueue().getSize() === 8, 'all items in queue');

    setTimeout(function() {
      flow.addList([6, 7, 8]);
    }, 20);
  }

  /*
   - generates id's for each instance
   - attaches itself to message brokers
   - can detach from specific message broker
   - thus keeps track of message brokers to which it is attached
   - knows how to handle items as well as errors
   - can recognize errors by instanceof, thus same method can be used
   for notifying observer of items and errors

   API:
   - addBroker(broker)
   - removeBroker(broker)
   - notify(msg)
   */
  FnFlow.Observer = (function() {
    var DEFAULT_PROPERTIES = {
      _brokerMap: null,
      _id: null,
      _results: null,
      _removeBrokersOnError: true,
      onComplete: function() {},
      onError: function() {},
      onResult: function() {}
    };

    function Observer(config) {
      setProperties(this, DEFAULT_PROPERTIES, config);
      this.setResults([]);
      this.setId(Observer.getNextId());
      this.setBrokerMap(new LinkedHashMap());
    }

    Observer.currentId = 0;

    Observer.getNextId = function() {
      return Observer.currentId++ % INT_MODULO;
    }

    Observer.prototype = {
      addBroker: function(broker) {
        if (typeof broker.removeObserver !== "function"
          || typeof broker.getId !== "function") {
          throw new FnFlow.Error("A broker must implement removeObserver() and getId()");
        }

        this.getBrokerMap()
          .put(broker.getId(), broker);
      },

      removeBroker: function(broker) {
        this.getBrokerMap()
          .remove(broker.getId());
      },

      removeAllBrokers: function() {
        var toRemove = [];
        var brokerMap = this.getBrokerMap();

        brokerMap.forEach(function(broker) {
          toRemove.push(broker.getId());
        });

        for(var i = toRemove.length; i--;) {
          brokerMap.remove(toRemove[i]);
        }
      },

      tryCatch: function(callback) {
        try {
          callback();
        } catch(err) {
          if (this.getRemoveBrokersOnError()) {
            this.removeAllBrokers();
          }

          // todo: only needed in chrome? not needed it nodeJs, since it seems to preserve original
          // stack on error rethrow.
          //          console.error ? console.error('Original stack:', err.stack) :
          //            console.log('Original stack:', err.stack);

          throw FnFlow.Error.fromError(err);
        }
      },

      notifyComplete: function() {
        this.tryCatch(function() {
          this.getOnComplete()(this.getResults());
        }.bind(this));
      },

      notifyError: function(err) {
        this.tryCatch(function() {
          this.getOnError()(err);
        }.bind(this));
      },

      notifyResult: function(result) {
        this.tryCatch(function() {
          this.getResults().push(result);
          this.getOnResult()(result);
        }.bind(this));
      }
    }

    bootstrapConstructor(Observer, Object.keys(DEFAULT_PROPERTIES));

    return Observer;
  })();

  function runTests() {
    for(var prop in tests) {
      console.log("Running tests for", prop);
      tests[prop]();
    }
  }
  //  runTests();

  return FnFlow;
})();

/**
 * node js compatibility
 */
if (typeof module !== 'undefined' && module.exports) {
  module.exports = FnFlow;
}