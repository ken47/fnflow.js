Available Under the MIT License

=====================

Work in progress. Not battle-tested, use at your own risk.

Ultimately intended to be a complement to ReactJS or similar libraries,
functionally mold your data, through chainable methods, flattened asynchronous callbacks,
and composable flows.

Tried RxJs extensively, but was very awkward getting it to do what I wanted.
Likewise with Bacon.js.

=====================

TODOs
a sensible default onError for observers

pass back original value with result/error

automatically detach a source/target flow if invoking it causes
an unexpected error

create FnFlow.prototype.addError/Complete(?), so that sources can also
stream errors to their targets

add condition parameter when adding source and target flow

flow -> process
- should also be able to set initial state of all child processes

add concurrency strategy where new items are always processed immediately,
and oldest processed are killed

onKill parameter for async operations.

Entity.getById(id) as a static method?
- would require a lot of memory

support for timeouts for specific async steps

take logic subject to race conditions, when concurrency > take
- split sequences at the take level
- need to be able to pause / resume threads at the take level
- optimistic, assume threads are likely to succeed.
  - block on a take step until the condition is satisfied. spawn next process when stuck on take

takeUntil…think about after implementing the above

indicate that an operation is dependent on the same operation within other processes
through a flag in the variable, such as adFilterByDuplicates (async, dependent) or
just don't allow such dependent operations at all...

filterDuplicates and throttle
- at the flow level or process level?
  - simplest implementation is flow level
- for filterDupes, if at process level, need some form of interprocess communication,
  either explicitly or implicitly through the parent, via idx. for example, can message
  process(pid - 1) to message the previous process, and ask for a response when
  it is killed or finished.
  - but this specific feature would require communicating with a specific operation
    within a specific process.

memoization
- this seems related to 'filterDuplicate' functionality
- memoization needs to occur within an Op instance
- startMemoize
- endMemoize
- Builder can validate using stack
- if the key for the memo is not a number/string by default, must take a function to generate key

garbage collection
- build upon deconstruct
- ensure mutual removal of brokers

continueOnError
- should probably affect child processes

what about flatMap?
- seems similar to take, above

support for unordered results

ensure usage of INT\_MODULO wherever applicable

keep track of path a piece of data has taken through flow graph
for debugging purposes
